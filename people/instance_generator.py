
from django.contrib.auth.models import User
from random import randint
from faculties.models import Faculty

PROFESSOR = '1'
ASSOCIATE_PROFESSOR = '2'
ASSISTANT_PROFESSOR = '3'
LECTURER = '4'
RANKS = (
    (PROFESSOR, 'Professor'),
    (ASSOCIATE_PROFESSOR, 'Associate Professor'),
    (ASSISTANT_PROFESSOR, 'Assistant Professor'),
    (LECTURER, 'Lecturer')
)


def people_generator(number_of_instances=50):
    k = 0
    User.objects.create_superuser(username='alouvoul', password='Scholarity2020', email='example@scholarity.org')
    saved_faculties = Faculty.objects.all()

    for i in range(number_of_instances):
        k += 1
        user = User(
            username="username " + str(k),
            email="email " + str(k),
            first_name="firstname " + str(k),
            last_name="lastname " + str(k),
        )
        user.save()

        # Create his author details
        user.author.citedby = randint(0, 50000)
        user.author.rank = RANKS[randint(0, 3)]
        user.author.publications = randint(0, 200)
        user.author.h_index = randint(0, 150)
        user.author.i_10_index = randint(0, 100)
        user.author.m_index = randint(0, 5)
        user.author.max_citations_in_publication = randint(0, 30000)
        user.author.faculty = saved_faculties[randint(0, len(saved_faculties)-1)]
        user.author.phd_year = "2017-07-05"
        user.author.google_scholar_url = None
        user.author.publication_with_max_citations = "publication " + str(k)
        user.author.bio_published = True
        user.author.profile_photo = None
        user.save()
        # author.google_scholar_name="None",
        # author.time_to_update = 0,
        # author.last_update = datetime.date.today,

    # author.save()
