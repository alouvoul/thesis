from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse

from django.dispatch import receiver
from django.db.models.signals import post_save

from faculties.models import Faculty

import datetime


class Author(models.Model):
    # Linked with a built-in User object
    user = models.OneToOneField(User, models.CASCADE, null=True)

    PROFESSOR = '1'
    ASSOCIATE_PROFESSOR = '2'
    ASSISTANT_PROFESSOR = '3'
    LECTURER = '4'
    RANKS = (
        (PROFESSOR, 'Professor'),
        (ASSOCIATE_PROFESSOR, 'Associate Professor'),
        (ASSISTANT_PROFESSOR, 'Assistant Professor'),
        (LECTURER, 'Lecturer')
    )
    rank = models.CharField(
        max_length=100,
        choices=RANKS,
    )
    citedby = models.PositiveIntegerField(default=0)
    faculty = models.ForeignKey(Faculty, null=True, on_delete=models.SET_NULL)
    publications = models.PositiveIntegerField(default=0)
    h_index = models.PositiveIntegerField(default=0,)
    i_10_index = models.PositiveIntegerField(default=0)
    m_index = models.FloatField(default=0)
    max_citations_in_publication = models.PositiveIntegerField(default=0)
    phd_year = models.DateField('Phd Year', null=True)
    google_scholar_url = models.CharField(max_length=500, null=True)
    publication_with_max_citations = models.CharField(max_length=500)
    bio_published = models.BooleanField(default=False)
    profile_photo = models.ImageField(upload_to='profile_pics/', blank=True)

    google_scholar_name = models.CharField(max_length=100, null=False)
    time_to_update = models.PositiveIntegerField(default=0, null=False)
    last_update = models.DateField(default=datetime.date.today)

    def get_absolute_url(self):
        return reverse('people:details', kwargs={'pk': self.pk})

    def __str__(self):
        return str(self.user.first_name) + ' ' + str(self.user.last_name)

    # A signal to create an Author object when User signup
    @receiver(post_save, sender=User)
    def update_user_author(sender, instance, created, **kwargs):
        if created:
            Author.objects.create(user=instance)
        instance.author.save()
