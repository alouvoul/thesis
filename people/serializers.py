from rest_framework import serializers
from .models import Author


class AuthorSerializer(serializers.ModelSerializer):

    class Meta:
        model = Author
        fields = [
            'id',
            'citedby',
            'rank',
            'publications',
            'h_index',
            'i_10_index',
            'm_index',
            'max_citations_in_publication',
            'phd_year',
            'google_scholar_url',
            'publication_with_max_citations',
            'bio_published',
        ]
