from django.urls import path
from . import views


app_name = 'people'
urlpatterns = [
    path('', views.index, name='index'),
    path('results/', views.results, name='results'),
    path('update-profile/<int:pk>/', views.ProfileUpdateView.as_view(), name='update'),
    path('auto-update-profile/<int:pk>/', views.auto_profile_update, name='auto-update'),
    # path('create-profile/', views.ProfileUpdateView.as_view(), name='create'),
    path('<int:pk>/', views.DetailsDetailView.as_view(), name='details'),
    path('delete/<int:pk>/', views.ProfileDeleteView.as_view(), name='delete'),

    # AJAX
    path('ajax/get_schools/', views.get_schools, name='get_schools'),
    path('ajax/get_departments/', views.get_departments, name='get_departments')
]

