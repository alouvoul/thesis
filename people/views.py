import scholarly
import threading
import time
import datetime

from datetime import timedelta
from django.shortcuts import render, HttpResponseRedirect
from django.http import Http404
from django.urls import reverse_lazy, reverse
from django.views import generic
from django.db.utils import OperationalError
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User

from .models import Author
from faculties.models import Faculty
from .forms import AuthorForm, AutoUpdateForm, DeleteAuthorForm

# REST API #
from rest_framework.views import APIView
from .serializers import AuthorSerializer
from rest_framework.response import Response


def index(request):

    top_10_authors = Author.objects.all().order_by('-citedby').exclude(user__is_staff=True).exclude(user__is_active=False)[:10]
    context = {
        'people': top_10_authors,
    }
    return render(request, 'people/index.html', context)


class ProfileUpdateView(LoginRequiredMixin, generic.UpdateView):
    model = Author
    form_class = AuthorForm
    template_name = 'people/update_profile.html'

    def post(self, request, *args, **kwargs):
        form = AuthorForm(request.POST, request.FILES)
        if form.is_valid():
            faculty = None
            try:
                faculty = Faculty.objects.get(
                    department=form.cleaned_data['department'],
                    school=form.cleaned_data['school'],
                    university=form.cleaned_data['university'],
                )
            except:
                print('Faculty is not exists')
            if faculty is not None:
                author = Author.objects.get(pk=request.user.id)
                author.rank = form.cleaned_data['rank']
                author.publications = form.cleaned_data['publications']
                author.citedby = form.cleaned_data['citedby']
                author.h_index = form.cleaned_data['h_index']
                author.i_10_index = form.cleaned_data['i_10_index']
                author.m_index = form.cleaned_data['m_index']
                author.max_citations_in_publication = form.cleaned_data['max_citations_in_publication']
                author.phd_year = form.cleaned_data['phd_year']
                author.google_scholar_url = form.cleaned_data['google_scholar_url']
                author.publication_with_max_citations = form.cleaned_data['publication_with_max_citations']
                author.bio_published = form.cleaned_data['bio_published']
                author.profile_photo = form.cleaned_data['profile_photo']
                author.faculty = faculty

                # Update Last update value
                author.last_update = datetime.date.today()
                # User in detail template to check who update last time
                author.google_scholar_name = ""
                author.save()

                return render(request, 'people/details.html', {'object': author})
        context = {
            'form': form,
            'object': request.user.author,
            'university': Faculty.objects.values_list('university', flat=True).distinct(),
            'school': Faculty.objects.values_list('school', flat=True).distinct(),
            'department': Faculty.objects.values_list('department', flat=True).distinct()
        }
        print(form.errors)
        return render(request, 'people/update_profile.html', context)

    def get_context_data(self, **kwargs):
        context = super(ProfileUpdateView, self).get_context_data(**kwargs)
        university = Faculty.objects.values_list('university', flat=True).distinct()
        # school = Faculty.objects.values_list('school', flat=True).distinct()
        # faculty = Faculty.objects.values_list('department', flat=True).distinct()

        # context['faculties'] = Faculty.objects.all()
        context['university'] = university
        # context['school'] = school
        # context['department'] = faculty
        return context


def get_schools(request):
    from django.http import JsonResponse
    university = request.GET.get('university')

    schools = Faculty.objects.filter(university=university).values_list('school', flat=True).distinct()
    data = {
        'schools': list(schools)
    }
    return JsonResponse(data)


def get_departments(request):
    from django.http import JsonResponse
    school = request.GET.get('school')
    university = request.GET.get('university')

    # Filter university & school because if retrieve only school probably there are a few schools with the same name
    departments = Faculty.objects.filter(school=school, university=university).values_list('department', flat=True).distinct()
    data = {
        'departments': list(departments)
    }

    return JsonResponse(data)


def auto_profile_update(request, pk):

    if request.method == 'POST':
        form = AutoUpdateForm(request.POST)
        print(form.errors)
        if form.is_valid():
            name = request.POST.get("auto_update")
            check_time = request.POST.get("checktime")
            repeat_update = request.POST.get("repeat_update")

            if check_time is None:
                check_time = 0
            check_time = int(check_time)

            author = online_search(name, pk)
            if author:
                author.google_scholar_name = name
                author.time_to_update = check_time
                author.save()
                return HttpResponseRedirect(reverse('people:details', kwargs={'pk': pk}))
            else:
                msg = 'Service unavailable! Please try again later.'
                form.add_error('auto_update', msg)
                return render(request, 'people/auto_update.html', {'form': form})
        else:
            print('Is not valid')
            return render(request, 'people/auto_update.html', {'form': form})
    else:
        form = AutoUpdateForm()
        return render(request, 'people/auto_update.html', {'form': form})


def online_search(name, pk):
    try:
        search = scholarly.scholarly.search_author(name)
        author = Author.objects.get(pk=pk)

        person_info = next(search).fill()
        print(person_info.name)
        print(person_info.citedby)

        author.citedby = person_info.citedby

        author.publications = len(person_info.publications)  # number of publications
        author.h_index = person_info.hindex
        author.i_10_index = person_info.i10index
        author.m_index = "{0:.2f}".format(float(person_info.hindex) / float(len(person_info.cites_per_year)))
        publication_info = person_info.publications[0].fill()
        author.max_citations_in_publication = sum(publication_info.cites_per_year.values())
        author.publication_with_max_citations = publication_info.bib['title']
        # Update Last update value
        author.last_update = datetime.date.today()
        return author
    except:
        return False


def time_interval_update():
    while True:
        time.sleep(86400)   # Repeat every day
        try:
            users_to_update = Author.objects.filter(time_to_update__gte=1).values_list('id', flat=True)
        except OperationalError:
            users_to_update = None

        if users_to_update is not None:
            for i in users_to_update:
                author = Author.objects.get(id=i)
                if its_time_to_update(author.last_update, author.time_to_update):
                    online_search(author.google_scholar_name, author.pk)


def its_time_to_update(last_update, time_to_update):
    if last_update < datetime.date.today():
        if last_update + timedelta(days=time_to_update) < datetime.date.today():
            return True
    else:
        return False


class ProfileDeleteView(LoginRequiredMixin, generic.DeleteView):
    model = Author
    success_url = reverse_lazy('people:index')
    template_name = 'people/author_confirm_delete.html'

    def get_context_data(self, **kwargs):
        context = super(ProfileDeleteView, self).get_context_data(**kwargs)
        context['form'] = DeleteAuthorForm()
        return context

    def delete(self, request, *args, **kwargs):
        from django.contrib.auth import authenticate
        from django.contrib import messages

        form = DeleteAuthorForm(request.POST)
        if form.is_valid():
            username = request.user.username
            password = form.cleaned_data.get('password')
            authenticated = authenticate(username=username, password=password)

            if authenticated:
                author_id = request.user.id
                delete_author = Author.objects.get(id=author_id)
                delete_user = User.objects.get(id=author_id)

                delete_user.delete()
                delete_author.delete()
                return HttpResponseRedirect(self.success_url)

        messages.add_message(request, messages.ERROR, 'Could not authenticate your profile')
        return render(request, self.template_name, {'object': request.user})


def results(request):
    """
    TODO Add some logic to find the correct name
    :param request:
    :return:
    """
    if request.method == 'GET':
        user_search = request.GET.get('query')
        try:
            authors = User.objects.none()
            for search in user_search:
                last = User.objects.filter(last_name__icontains=search).order_by('author__citedby').exclude(is_staff=True).exclude(is_active=False)
                first = User.objects.filter(first_name__icontains=search).order_by('author__citedby').exclude(is_staff=True).exclude(is_active=False)
                authors = authors | last | first
            if user_search == '':
                authors = User.objects.all().exclude(is_staff=True).exclude(is_active=False)
        except User.DoesNotExist:
            raise Http404('Author doesn \'t exist')
        context = {
            'authors': authors.order_by('last_name'),
        }
        return render(request, 'people/results.html', context)


class DetailsDetailView(generic.DetailView):
    model = Author
    template_name = 'people/details.html'


class AuthorList(APIView):

    def get(self, request):
        authors = Author.objects.all()
        serializer = AuthorSerializer(authors, many=True)
        return Response(serializer.data)

    def post(self):
        pass


# Thread calls for auto update user details
print('Start Thread')
x = threading.Thread(target=time_interval_update)
x.start()
