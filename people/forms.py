from django import forms
from people.models import Author
from faculties.models import Faculty


class AuthorForm(forms.ModelForm):

    department = forms.CharField()
    university = forms.CharField()
    school = forms.CharField()

    class Meta:
        model = Author
        fields = [
            'citedby',
            'rank',
            'publications',
            'h_index',
            'i_10_index',
            'm_index',
            'max_citations_in_publication',
            'phd_year',
            'google_scholar_url',
            'publication_with_max_citations',
            'bio_published',
            'profile_photo',
        ]
        help_texts = {
            'citedby': 'Total umber of citations',
            'h_index': 'N Publications with at least N citations',
            'i_10_index': 'Number of publications with 10 or more citations',
            'm_index': 'Metric of h/years',
            'max_citations_in_publication': 'Max number of citations in a single publication'
        }

    def save(self, commit=True):
        faculty = Faculty.objects.values(
            department=self.cleaned_data['department'],
            school=self.cleaned_data['school'],
            university=self.cleaned_data['university'],
        )
        print("Save method")
        print(faculty.department)
        self.cleaned_data['faculty'] = faculty.id
        return super(AuthorForm, self).save(commit)


class AutoUpdateForm(forms.Form):
    auto_update = forms.CharField(max_length=100)
    checktime = forms.IntegerField(min_value=0)
    repeat_update = forms.BooleanField(required=False)


class DeleteAuthorForm(forms.Form):
    password = forms.CharField(widget=forms.PasswordInput)
