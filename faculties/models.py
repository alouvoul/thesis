from django.db import models
from django.urls import reverse

from faculties.conf import UNI


class Tag(models.Model):
    word = models.CharField(max_length=100, null=True)
    # slug = models.SlugField(max_length=40)

    def __str__(self):
        return self.word

    def get_absolute_url(self):
        return reverse('faculties:index')


class Faculty(models.Model):

    class Meta:
        permissions = (
            ('can_update', 'Can Update'),
            ('can_delete', 'Can Delete'),
        )

    department = models.CharField(max_length=100)
    school = models.CharField(max_length=100)
    university = models.CharField(
        max_length=100,
        choices=UNI,
    )
    science_type = models.ManyToManyField(Tag, blank=True)
    url_site = models.URLField(max_length=250)
    description = models.CharField(max_length=1000, blank=True)

    def get_absolute_url(self):
        return reverse('faculties:detail', kwargs={'faculty_id': self.pk})

    def __str__(self):
        return str(self.department) + ' - ' + str(self.university)

    def get_university(self):
        return self.university

    def get_school(self):
        return self.school

    def get_faculty(self):
        return self.department


class FacultyApplication(Faculty):

    application_date = models.DateTimeField(null=False)
    handled = models.BooleanField(default=False)
    submit_person = models.CharField(max_length=100)
