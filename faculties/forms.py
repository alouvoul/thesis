from django.forms import ModelForm
from .models import Faculty, Tag, FacultyApplication


class FacultyForm(ModelForm):

    class Meta:
        model = Faculty
        fields = [
                'department',
                'school',
                'university',
                'science_type',
                'url_site',
                'description',
        ]


class TagForm(ModelForm):

    class Meta:
        model = Tag
        fields = [
            'word',
        ]


class FacultyApplicationForm(ModelForm):
    class Meta:
        model = FacultyApplication
        fields = [
            'department',
            'school',
            'university',
            'science_type',
            'url_site',
            'description'
        ]
