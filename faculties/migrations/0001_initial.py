# Generated by Django 2.2 on 2019-10-13 21:44

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Faculty',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('department', models.CharField(max_length=100)),
                ('school', models.CharField(max_length=100)),
                ('university', models.CharField(choices=[('University of western macedonia', 'University of western macedonia'), ('National technical university of Athens', 'National technical university of Athens'), ('Aristotle university', 'Aristotle university'), ('University of Macedonia', 'University of Macedonia'), ('Democritus university of Thrace', 'Democritus university of Thrace'), ('University of Thessaly', 'University of Thessaly'), ('University of Crete', 'University of Crete'), ('University of Patra', 'University of Patra'), ('University of Aegean', 'University of Aegean'), ('University of Ioannina', 'University of Ioannina'), ('University of Piraeus', 'University of Piraeus'), ('National and Kapodistrian University of Athens', 'National and Kapodistrian University of Athens'), ('University of Arts', 'University of Arts'), ('Panteion University', 'Panteion University'), ('Xarokopio University', 'Xarokopio University'), ('University of Cyprus', 'University of Cyprus'), ('Agricultural University of Athens', 'Agricultural University of Athens'), ('Open University', 'Open University'), ('Ionio University', 'Ionio University'), ('Athens University of economics and business', 'Athens University of economics and business'), ('University of Peloponnese', 'University of Peloponnese')], max_length=100)),
                ('url_site', models.URLField(max_length=250)),
            ],
            options={
                'permissions': (('can_update', 'Can Update'), ('can_delete', 'Can Delete')),
            },
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('word', models.CharField(max_length=100, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='FacultyApplication',
            fields=[
                ('faculty_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='faculties.Faculty')),
                ('application_date', models.DateTimeField()),
                ('handled', models.BooleanField(default=False)),
                ('submit_person', models.CharField(max_length=100)),
            ],
            bases=('faculties.faculty',),
        ),
        migrations.AddField(
            model_name='faculty',
            name='science_type',
            field=models.ManyToManyField(blank=True, to='faculties.Tag'),
        ),
    ]
