from rest_framework import serializers
from .models import Faculty, Tag


class FacultySerializer(serializers.ModelSerializer):

    class Meta:
        model = Faculty
        fields = [
                'department',
                'school',
                'university',
                'science_type',
                'url_site',
        ]


class TagSerializer(serializers.ModelSerializer):

    class Meta:
        model = Tag
        fields = '__all__'
