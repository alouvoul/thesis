from django.contrib import admin
from .models import Faculty, Tag, FacultyApplication

from guardian.admin import GuardedModelAdmin


class FacultyAdmin(GuardedModelAdmin):
    pass


admin.site.register(Tag)
admin.site.register(Faculty, FacultyAdmin)
admin.site.register(FacultyApplication)