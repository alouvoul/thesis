from django.urls import path
from . import views

app_name = 'faculties'
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('results/', views.results, name='results'),
    path('detail/<int:faculty_id>/', views.detail, name='detail'),
    path('create_faculty/', views.CreateFacultyView.as_view(), name='create_faculty'),
    path('update/<int:pk>/', views.UpdateFacultyView.as_view(), name='update_faculty'),
    path('delete_faculty/<int:pk>', views.DeleteFacultyView.as_view(), name='delete_faculty'),

    path('create_tag/', views.CreateTagView.as_view(), name='create_tag'),
    path('application-submit/', views.application_submit, name='submit-form'),
]
