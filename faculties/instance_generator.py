from random import randint

from faculties.models import Faculty
from faculties.conf import UNI

description = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s ' \
              'standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to ' \
              'make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, ' \
              'remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing ' \
              'Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions ' \
              'of Lorem Ipsum.'


def faculty_generator(number_of_instances=20):

    for i in range(number_of_instances):

        faculty = Faculty(
            department="department_" + str(i),
            school="School_" + str(i),
            university=UNI[randint(0, len(UNI)-1)][1],
            url_site="example@example.com",
            description=description,
        )
        faculty.save()


