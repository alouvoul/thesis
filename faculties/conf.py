UNI = (
        ('University of western macedonia', 'University of western macedonia'),
        ('National technical university of Athens', 'National technical university of Athens'),
        ('Aristotle university', 'Aristotle university'),
        ('University of Macedonia', 'University of Macedonia'),
        ('Democritus university of Thrace', 'Democritus university of Thrace'),
        ('University of Thessaly', 'University of Thessaly'),
        ('University of Crete', 'University of Crete'),
        ('University of Patra', 'University of Patra'),
        ('University of Aegean', 'University of Aegean'),
        ('University of Ioannina', 'University of Ioannina'),
        ('University of Piraeus', 'University of Piraeus'),
        ('National and Kapodistrian University of Athens', 'National and Kapodistrian University of Athens'),
        ('University of Arts', 'University of Arts'),
        ('Panteion University', 'Panteion University'),
        ('Xarokopio University', 'Xarokopio University'),
        ('University of Cyprus', 'University of Cyprus'),
        ('Agricultural University of Athens', 'Agricultural University of Athens'),
        ('Open University', 'Open University'),
        ('Ionio University', 'Ionio University'),
        ('Athens University of economics and business', 'Athens University of economics and business'),
        ('University of Peloponnese', 'University of Peloponnese'),
    )

# UNIVERSITY_OF_WESTERN_MACEDONIA = 'University of western macedonia'
# NATIONAL_TECHNICAL_UNIVERSITY_OF_ATHENS = 'National technical university of Athens'
# ARISTOTLE_UNIVERSITY = 'Aristotle university'
# UNIVERSITY_OF_MACEDONIA = '4'
# DEMOCRITUS_UNIVERSITY_OF_THRACE = '5'
# UNIVERSITY_OF_THESSALY = '6'
# UNIVERSITY_OF_CRETE = '7'
# UNIVERSITY_OF_PATRA = '8'
# UNIVERSITY_OF_AEGEAN = '9'
# UNIVERSITY_OF_IOANNINA = '10'
# UNIVERSITY_OF_PIRAEUS = '11'
# NATIONAL_AND_KAPODISTRIAN_UNIVERSITY_OF_ATHENS = '12'
# UNIVERSITY_OF_ARTS = '13'
# PANTEION_UNIVERSITY = '14'
# XAROKOPIO_UNIVERSITY = '15'
# UNIVERSITY_OF_CYPRUS = '16'
# AGRICULTURAL_UNIVERSITY_OF_ATHENS = '17'
# OPEN_UNIVERSITY = '18'
# IONIO_UNIVERSITY = '19'
# ATHENS_UNIVERSITY_OF_ECONOMICS_AND_BUSINESS = '20'
# UNIVERSITY_OF_PELOPONNESE = '21'