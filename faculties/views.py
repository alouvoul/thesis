from django.shortcuts import render, Http404, HttpResponse
from django.views.generic import TemplateView, CreateView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.db.models import Sum
from django.db.models.functions import Coalesce

import datetime

from guardian.shortcuts import assign_perm
from guardian.mixins import PermissionRequiredMixin

from .models import Faculty, Tag, FacultyApplication
from .forms import FacultyForm, TagForm, FacultyApplicationForm
from people.models import Author

from rest_framework.response import Response
from rest_framework.views import APIView
from .serializers import FacultySerializer, TagSerializer


class IndexView(TemplateView):
    template_name = 'faculties/index.html'
    context_object_name = 'faculty_list'

    def get_context_data(self, **kwargs):
        """
        Function that reserves Template data. Retrieve 5 departments with most citations of authors. If there is no data
        to an author 0 added.
        :param kwargs:
        :return: data to the template
        """
        context = super().get_context_data(**kwargs)
        context['faculty_list'] = Faculty.objects.annotate(cited=Coalesce(Sum('author__citedby'), 0)).order_by('-cited')[:5]
        context['faculties_with_most_publications'] = Faculty.objects.annotate(publications=Coalesce(Sum('author__publications'), 0))

        return context


class CreateFacultyView(LoginRequiredMixin, CreateView):
    form_class = FacultyForm
    template_name = 'faculties/create.html'

    def get(self, request, *args, **kwargs):
        tags = Tag.objects.all()
        context = {
            'form': self.form_class,
            'tags': tags,
        }
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        form = FacultyApplicationForm(request.POST)

        if form.is_valid():
            new_application = FacultyApplication()
            new_application.department = form.cleaned_data['department']
            new_application.school = form.cleaned_data['school']
            new_application.university = form.cleaned_data['university']
            new_application.url_site = form.cleaned_data['url_site']

            word = request.POST.getlist('science_type')
            for i in word:
                tag = Tag.objects.get(pk=i)
                new_application.science_type.add(tag)
            new_application.application_date = datetime.datetime.today()
            new_application.submit_person = request.user
            new_application.save()

            return HttpResponse('/application-submit')


def application_submit(request):
    HttpResponse()


class UpdateFacultyView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    model = Faculty
    form_class = FacultyForm
    template_name = 'faculties/update.html'
    permission_required = 'can_update'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['faculty_id'] = self.object.pk
        context['tags'] = Tag.objects.all()

        faculty = Faculty.objects.get(pk=self.object.pk)
        context['faculty'] = faculty
        return context

    def form_valid(self, form):
        if form.save(self):
            return super(UpdateFacultyView, self).form_valid(form)
        else:
            return self

    def post(self, request, *args, **kwargs):
        form = FacultyForm(request.POST)

        for i in form.errors:
            print(i)

        if form.is_valid():
            faculty = Faculty.objects.get(pk=kwargs['pk'])

            faculty.department = form.cleaned_data['department']
            faculty.school = form.cleaned_data['school']
            faculty.university = form.cleaned_data['university']
            faculty.url_site = form.cleaned_data['url_site']
            word = request.POST.getlist('science_type')
            faculty.science_type.clear()
            for i in word:
                tag = Tag.objects.get(pk=i)
                faculty.science_type.add(tag)
            faculty.save()

            # Context to preview details
            context = {
                'faculty': faculty,
                'tags': faculty.science_type.all()
            }
            return render(request, 'faculties/detail.html', context)
        else:
            return render(request, 'faculties/update.html', {'form': form})


class DeleteFacultyView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    model = Faculty
    success_url = reverse_lazy('faculties:index')
    permission_required = 'can_delete'


def results(request):
    if request.method == 'GET':
        user_search = request.GET.get('query')
        try:
            faculties = Faculty.objects.none()
            for search in user_search:
                department = Faculty.objects.filter(department__icontains=search).annotate(cited=Coalesce(Sum('author__citedby'), 0),
                    h_index=Coalesce(Sum('author__h_index'), 0),
                    publications=Coalesce(Sum('author__publications'), 0),)
                school = Faculty.objects.filter(school__icontains=search).annotate(cited=Coalesce(Sum('author__citedby'), 0),
                    h_index=Coalesce(Sum('author__h_index'), 0),
                    publications=Coalesce(Sum('author__publications'), 0),)
                university = Faculty.objects.filter(university__icontains=search).annotate(
                    cited=Coalesce(Sum('author__citedby'), 0),
                    h_index=Coalesce(Sum('author__h_index'), 0),
                    publications=Coalesce(Sum('author__publications'), 0),)
                faculties = faculties | department | school | university
            # If there is no user input return all faculties
            if user_search == '':
                faculties = Faculty.objects.all().annotate(cited=Coalesce(Sum('author__citedby'), 0),
                    h_index=Coalesce(Sum('author__h_index'), 0),
                    publications=Coalesce(Sum('author__publications'), 0),)

        except Faculty.DoesNotExist:
            raise Http404('Faculty doesn \'t exist')
        context = {
            'faculties': faculties.order_by('department'),
        }
        return render(request, 'faculties/results.html', context)


def detail(request, faculty_id):
    try:
        faculty = Faculty.objects.get(id=faculty_id)
        tags = faculty.science_type.all()
        people = Author.objects.filter(faculty=faculty)
        for i in tags:
            print(i)
        context = {
            'faculty': faculty,
            'tags': tags,
            'people': people,
        }
    except Faculty.DoesNotExist:
        raise Http404("Cant retrieve this thing")
    return render(request, 'faculties/detail.html', context)


class CreateTagView(CreateView):
    form_class = TagForm
    template_name = 'faculties/create_tag.html'


class FacultyList(APIView):

    def get(self, request):
        faculties = Faculty.objects.all()
        serializer = FacultySerializer(faculties, many=True)
        return Response(serializer.data)

    def post(self):
        pass


class TagList(APIView):

    def get(self, request):
        tag = Tag.objects.all()
        serializer = TagSerializer(tag, many=True)
        return Response(serializer.data)

    def post(self):
        pass
