"""bibliometrics URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

# For development
from django.conf import settings
from django.conf.urls.static import static

from rest_framework.urlpatterns import format_suffix_patterns
from people.views import AuthorList
from faculties.views import FacultyList, TagList

urlpatterns = [
    # Built-in apps
    path('admin/', admin.site.urls),

    # Custom app needs to be first
    # Includes only built-in URLS
    path('accounts/', include('accounts.urls')),
    path('accounts/', include('django.contrib.auth.urls')),

    # Project home page
    path('', include('home.urls')),

    # Custom apps
    path('faculties/', include('faculties.urls')),
    path('people/', include('people.urls')),
    path('analytics/', include('analytics.urls')),

    # REST API
    path('api_authors/', AuthorList.as_view()),
    path('api_faculties/', FacultyList.as_view()),
    path('api_tag/', TagList.as_view()),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

# if settings.DEBUG: # new

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns = format_suffix_patterns(urlpatterns)
