from people.instance_generator import people_generator
from faculties.instance_generator import faculty_generator


def generate_objects():
    print("Creating faculty instances...\n")
    faculty_generator()
    print("Creating author instances...\n")
    people_generator()



