from django.http import HttpResponse
from reportlab.platypus import Paragraph
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.platypus import Table, TableStyle, Spacer
from reportlab.platypus.flowables import DocIf
PATH = './reports/'

SPACER_H = 30
SPACER_W = 30


def print_pdf_report(self, request, query1, query2, example1, example2):
    from time import strftime, gmtime
    from reportlab.graphics.shapes import Drawing, String
    from reportlab.platypus import SimpleDocTemplate
    from reportlab.lib.pagesizes import letter

    #################################################################################
    #################################################################################
    filename = PATH + "{filename}.pdf".format(filename=strftime("%Y-%m-%d %H:%M:%S", gmtime()))
    doc = SimpleDocTemplate(filename, pagesize=letter)
    elements = []

    # Paragraph
    header, ptext, a = create_text(query1, query2)
    elements.append(header)
    # Space
    elements.append(Spacer(SPACER_H, SPACER_W))
    elements.append(ptext)
    elements.append(a)

    # Space
    elements.append(Spacer(SPACER_H, SPACER_W))

    # Table
    t = create_table(query1, query2, example1, example2)
    elements.append(t)

    # Space
    elements.append(Spacer(SPACER_H, SPACER_W))

    # Show LABEL for charts
    stylesheet = getSampleStyleSheet()
    header_style = stylesheet['Normal']
    label_a = '<para>' + 'A:' + query1 + '</para>'
    label_a = Paragraph(label_a, header_style, bulletText='-')
    label_b = '<para>' + 'B:' + query2 + '</para>'
    label_b = Paragraph(label_b, header_style, bulletText='-')
    elements.append(label_a)
    elements.append(label_b)

    # Drawing for charts
    d = Drawing(800, 400)

    # Citations Chart
    pc = create_pie_charts(200, 120, query1, query2, example1['citedby'], example2['citedby'])
    d.add(pc)
    d.add(String(70, 200, 'Citations Chart:', fontSize=14))

    # i_10_index Chart
    i_10_chart = create_pie_charts(200, 40, query1, query2, example1['i_10_index'], example2['i_10_index'])
    d.add(i_10_chart)
    d.add(String(70, 120, 'I-10 Index Chart:', fontSize=14))

    # H_index Chart
    i_10_chart = create_pie_charts(200, -40, query1, query2, example1['h_index'], example2['h_index'])
    d.add(i_10_chart)
    d.add(String(70, 40, 'H Index Chart:', fontSize=14))

    # Vertical bar chart
    vertical_bar_chart = create_bar_chart(100, 250, query1, query2, example1, example2)
    d.add(vertical_bar_chart)
    elements.append(d)

    doc.build(elements)
    return pdf_view(request, filename)


def create_text(query1, query2):
    stylesheet = getSampleStyleSheet()
    header = query1 + '-' + query2
    header_style = stylesheet['Heading1']

    header = Paragraph(header, header_style)

    text = 'Probably this is not gonna make it' \
           'asdadbhascuiydgf' \
           'asdifgh'

    normalStyle = stylesheet['Normal']
    ptext = Paragraph(text, normalStyle)

    a = DocIf('1>3', Paragraph('Citations of TASOS is larger than 3', normalStyle),
              Paragraph('The value of i is not larger than 3', normalStyle))

    return header, ptext, a


def create_table(query1, query2, example1, example2):

    data = [
        ['Name', 'Citations', 'H-Index', 'I-10 Index'],
        [query1, example1['citedby'], example1['i_10_index'], example1['h_index']],
        [query2, example2['citedby'], example2['i_10_index'], example2['h_index']]
    ]
    table = Table(data)
    return table


def create_pie_charts(
        x,
        y,
        query1,
        query2,
        number1,
        number2,
):
    from reportlab.graphics.charts.piecharts import Pie

    # Citations
    pc = Pie()
    pc.x = x
    pc.y = y
    pc.height = 70
    pc.width = 70
    pc.data = [number1, number2]
    pc.labels = [query1, query2]
    pc.slices.strokeWidth = 1
    pc.slices.popout = 1
    pc.sideLabels = 0
    return pc


def create_bar_chart(
        x,
        y,
        query1,
        query2,
        example1,
        example2,
):
    from reportlab.graphics.charts.barcharts import VerticalBarChart
    from reportlab.lib import colors
    # Vertical Bar Chart

    data = [
        (example1['m_index'], example1['i_10_index'], example1['h_index']),
        (example2['m_index'], example2['i_10_index'], example2['h_index'])
    ]
    bc = VerticalBarChart()
    bc.x = x
    bc.y = y
    bc.height = 125
    bc.width = 300
    bc.data = data
    bc.valueAxis.valueMin = 0
    bc.valueAxis.valueStep = 10
    bc.strokeColor = colors.black
    bc.categoryAxis.labels.boxAnchor = 'ne'
    bc.categoryAxis.labels.dx = 8
    bc.categoryAxis.labels.dy = -2
    bc.categoryAxis.labels.angle = 30
    bc.categoryAxis.categoryNames = [
        'M-Index',
        'I-10 Index',
        'H-Index',
    ]
    return bc


def pdf_view(request, filename):
    with open(filename, 'rb') as pdf:
        response = HttpResponse(pdf.read(), content_type='application/pdf')
        response['Content-Disposition'] = 'inline;filename={filename}.pdf'.format(filename=filename)
        return response
    pdf.closed
