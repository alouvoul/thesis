from django.shortcuts import render
from django.views import generic
from django.db.models import Sum
from django.db.models.functions import Coalesce

from faculties.models import Faculty
from .forms import AnalyticsForm
import collections


class IndexView(generic.TemplateView):
    template_name = 'analytics/index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Retrieve all faculties and user gonna filter the results
        faculties = {
            'university': [],
            'department': Faculty.objects.all().values_list('department', flat=True),
            'school': [],
            'science_type': [],
        }
        values = self.common_filtering(faculties)

        # Set each level to preview at default
        context['faculty'] = self.department_level(values)
        context['university'] = self.university_level(values)
        context['form'] = AnalyticsForm()
        return context

    def post(self, request):
        form = AnalyticsForm(request.POST)

        if form.is_valid():

            university = form.cleaned_data['university']
            school = form.cleaned_data['school']
            department = form.cleaned_data['department']
            science_type = form.cleaned_data['science_type']

            # Store the values
            values = {
                'university': university,
                'school': school,
                'department': department,
                'science_type': science_type,
            }
            faculties = self.common_filtering(values)
            query1 = self.university_level(faculties)
            query2 = self.department_level(faculties)
            print(query1)
            print('')
            print(query2)
            print('')
            query1 = self.sort_method(request, query1)
            query2 = self.sort_method(request, query2)
            context = {
                'university': query1,
                'faculty': query2,
                'form': form,
            }

            return render(request, 'analytics/index.html', context)
        else:
            print("Its not valid")
            return render(request, self.template_name, IndexView.get_context_data())

    @staticmethod
    def sort_method(request, query):
        sort = request.POST.get('hiddenField')

        try:
            if 'sort_by_university' == sort:
                sort = 'university'
                print(sort)
                query = collections.OrderedDict(sorted(query.items()))
            elif 'sort_by_department' == sort:
                sort = 'department'
                print(query)

            else:
                if 'sort_by_citations' in sort:
                    sort = 'cited'
                elif 'sort_by_publications' == sort:
                    sort = 'publications'
                elif 'sort_by_h_index' == sort:
                    sort = 'h_index'
                elif 'sort_by_i_10_index' == sort:
                    sort = 'i_10_index'
                elif 'sort_by_m_index' == sort:
                    sort = 'm_index'
                query = sorted(query, key=lambda k: query[k][sort])

            #sorted(query.items(), key=lambda x: x[1][sort], )
        except:
            print('GGWP')
        print("===========")
        print(query)
        return query


    @staticmethod
    def common_filtering(values):
        # Prepare the query with fields(exclude None field)
        query1 = {}
        for k, v in values.items():
            if v is not None:
                query1[k] = v

        # Query all faculty to filter by field
        faculties = Faculty.objects.all()
        if query1['university']:
            faculties = faculties.filter(university__in=query1['university'])
        if query1['school']:
            faculties = faculties.filter(school__in=query1['school'])
        if query1['department']:
            faculties = faculties.filter(department__in=query1['department'])
        if query1['science_type']:
            # = Tag.objects.filter(word__in=query1['science_type'])
            faculties = faculties.filter(science_type__word__in=query1['science_type'])

        return faculties

    @staticmethod
    def department_level(faculties):

        query = {}
        for i in faculties:
            query[i] = Faculty.objects.filter(id=i.id).annotate(
                cited=Coalesce(Sum('author__citedby'), 0),
                i_10_index=Coalesce(Sum('author__i_10_index'), 0),
                h_index=Coalesce(Sum('author__h_index'), 0),
                publications=Coalesce(Sum('author__publications'), 0),
                m_index=Coalesce(Sum('author__m_index'), 0)
            )
        return query

    @staticmethod
    def university_level(faculties):

        # Get only university names with distinct
        universities = faculties.values_list('university', flat=True).distinct()
        university = {}
        for i in universities:
            university[i] = faculties.filter(university=i).aggregate(
                cited=Coalesce(Sum('author__citedby'), 0),
                i_10_index=Coalesce(Sum('author__i_10_index'), 0),
                h_index=Coalesce(Sum('author__h_index'), 0),
                publications=Coalesce(Sum('author__publications'), 0),
                m_index=Coalesce(Sum('author__m_index'), 0)
            )
        return university
