from django.db.utils import OperationalError
from django import forms
from faculties.models import Faculty, Tag


class AnalyticsForm(forms.Form):
    try:
        university = forms.MultipleChoiceField(
                    widget=forms.CheckboxSelectMultiple,
                    choices=[[x, x] for x in Faculty.objects.values_list('university', flat=True).distinct()],
                    required=False,
        )
        school = forms.MultipleChoiceField(
            widget=forms.CheckboxSelectMultiple,
            choices=[[x, x] for x in Faculty.objects.values_list('school', flat=True).distinct()],
            required=False,
        )
        department = forms.MultipleChoiceField(
            widget=forms.CheckboxSelectMultiple,
            choices=[[x, x] for x in Faculty.objects.values_list('department', flat=True).distinct()],
            required=False,
        )
        science_type = forms.MultipleChoiceField(
            widget=forms.CheckboxSelectMultiple,
            choices=[[x, x] for x in Tag.objects.values_list('word', flat=True).distinct()],
            required=False,
        )
    except OperationalError:
        pass

    # def __init__(self, *args, **kwargs):
    #     faculty = kwargs.pop('faculty', None)
    #     super(AnalyticsForm, self).__init__(*args, **kwargs)
    #     self.fields['university'].choices = Faculty.objects.values_list('university', flat=True).distinct()
