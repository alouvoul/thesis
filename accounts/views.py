from django.views import generic
from django.urls import reverse_lazy
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from .forms import UserForm


# TODO Change form creation to add first name and last name
class SignUpView(generic.CreateView):
    form_class = UserForm
    success_url = reverse_lazy('login')
    template_name = 'signup.html'

    def post(self, request, *args, **kwargs):

        form = UserForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            # login(request, user)
            return redirect(self.success_url)
        else:

            return render(request, 'signup.html', {'form': form})
