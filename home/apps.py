import sys
from django.apps import AppConfig


class HomeConfig(AppConfig):
    name = 'home'

    def ready(self):
        argv = sys.argv
        if '--generate-instances' in argv:
            from bibliometrics.initializers import generate_objects
            generate_objects()
            print('Fake data generated....')
            exit(0)