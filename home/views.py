from django.views.generic import TemplateView
from django.shortcuts import render
from django.db.models import Sum
from django.db.models.functions import Coalesce


from django.contrib.auth.models import User
from faculties.models import Faculty
from people.models import Author


class HomeView(TemplateView):
    template_name = 'home/home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        top_5_departments = Faculty.objects.annotate(cited=Sum('author__citedby')).order_by('-cited')[:5]
        context['top_5_departments'] = top_5_departments
        context['top_5_authors'] = Author.objects.all().order_by('-citedby').exclude(user__is_staff=True).exclude(user__is_active=False)[:5]
        context['top_5_universities'] = top_5_departments.values_list('university', flat=True).annotate(cited=Coalesce(Sum('author__publications'), 0))[:5]
        return context

    def post(self, request):
        user_search = request.POST.get('input')
        user_search = user_search.split()

        faculties = Faculty.objects.none()
        authors = Author.objects.none()
        for search in user_search:
            department = Faculty.objects.filter(department__icontains=search).annotate(cited=Coalesce(Sum('author__citedby'), 0)).order_by('cited')
            school = Faculty.objects.filter(school__icontains=search).annotate(cited=Coalesce(Sum('author__citedby'), 0)).order_by('cited')
            university = Faculty.objects.filter(university__icontains=search).annotate(cited=Coalesce(Sum('author__citedby'), 0)).order_by('cited')
            faculties = faculties | department | school | university

            last = User.objects.filter(last_name__icontains=search).order_by('-author__citedby').exclude(is_staff=True).exclude(is_active=False)
            first = User.objects.filter(first_name__icontains=search).order_by('-author__citedby').exclude(is_staff=True).exclude(is_active=False)
            authors = authors | last | first

        context = {
            'authors': authors.order_by('-author__citedby'),
            'faculties': faculties.order_by('-cited'),
        }
        return render(request, 'home/array_template.html', context)

